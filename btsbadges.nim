# Debian BTS badges
# Copyright 2020 Federico Ceratto <federico@debian.org>
# Released under AGPLv3 License, see LICENSE file

import asyncdispatch,
  colors,
  jester,
  osproc,
  db_postgres,
  strutils,
  times

from posix import onSignal, SIGABRT

import morelogging


let log = newJournaldLogger()

const defaultsfile = "/etc/default/distrobadges"
const cache_control = "max-age=3600, public"
const svgheaders = [
  ("Cache-Control", cache_control),
  ("Content-Type", "image/svg+xml"),
]

# globals
var baseurl = "https://btsbadges.debian.net"
var db: DbConn

proc parse_defaults() =
  ## Set the baseurl global variable
  try:
    for line in defaultsfile.lines:
      if line.startswith("baseurl="):
        baseurl = line[8..^1]
        log.info("Setting baseurl '$#'" % baseurl)

  except:
    discard

type
  BadgeParams = ref object
    w0, w1, logoWidth, logoPadding: int
    color1, color2, logo, sbj, status: string

include "templates/index.tmpl"
include "templates/badge-flat.svg"

proc estimate_text_width(t: string): int =
  return int(t.len.float * 6.8)

proc render_badge(subject, status: string, color: Color): string =
  let color_left = "#404040"
  var b = BadgeParams(
    color1: color_left,
    color2: $color,
    logo: "",
    logoPadding: 3,
    sbj: subject,
    status: status,

  )
  # width of the left side
  b.w0 = b.sbj.estimate_text_width + 6 + b.logoWidth + b.logoPadding
  # width of the right side
  b.w1 = b.status.estimate_text_width + 12
  let badge = generate_badge(b)
  return badge

proc connect_to_db() =
  log.info("Connecting to the database")
  db = open("udd-mirror.debian.net", "udd-mirror",
            "udd-mirror", "udd")

template retry(body): untyped =
  try:
    body
  except DbError:
    log.error("DbError " & getCurrentExceptionMsg())
    connect_to_db()
    body

proc filter_user_input(inp: string): string =
  ## Filter user input
  const allowed = Letters + Digits + {'_', '-', '~'}
  result = newStringOfCap(inp.len)
  for c in inp:
    if c in allowed:
      result.add c
router myrouter:


  get "/":
    resp generate_index(baseurl, "nim", "", "all", "amd64", "")

  post "/":
    #let tokens = @"distro_and_flavour".split('|')
    let
      pname = filter_user_input(@"pname")
      caption = filter_user_input(@"caption")
      bugfilter = filter_user_input(@"bugfilter")
      arch = filter_user_input(@"arch")
      uh = filter_user_input(@"uh")

    resp generate_index(baseurl, pname, caption, bugfilter, arch, uh)


  get "/badges/bugs/@pname/@filter/?@text?":
    ## Get bug count by package
    let pname = @"pname"
    let count =
      if @"filter" == "rc":
        retry:
          db.getvalue(sql"SELECT rc_bugs FROM bugs_count WHERE source = ?", pname)
      else:
        retry:
          db.getvalue(sql"SELECT all_bugs FROM bugs_count WHERE source = ?", pname)

    let text = @"text"
    let sbj = if text != "": text else: "bugs"
    let badge =
      if count == "0":
        render_badge(sbj, count, colGreen)
      else:
        render_badge(sbj, count, colBlack)

    resp(Http200, svgheaders, badge)

  get "/favicon.ico":
    resp readFile("favicon.ico")

  get "/badges/wnpp/@pname/?@text?":
    ## Get WNPP status
    let pname = @"pname"
    let row = db.getRow(sql"SELECT type FROM wnpp WHERE source = ?", pname)
    let wnpp_status = row[0]
    const q = sql"SELECT COUNT(*) FROM sources WHERE source = ? AND release = 'sid'"

    let (status, color) =
      if wnpp_status != "":
        # There is a WNPP entry
        case wnpp_status
          of "O": ("orphaned", colRed)
          of "RFP": ("RFP", colOrange)
          of "ITP": ("ITP", colDarkGreen)
          else: (wnpp_status, colOrange)

      else:
        # not in WNPP: check if it's packaged in Sid
        retry:
          if db.getvalue(q, pname).parseInt() > 0:
            ("done", colLightGreen)
          else:
            ("none", colDarkGray)

    let text = @"text"
    let sbj = if text != "": text else: "wnpp"
    let badge = render_badge(sbj, status, color)
    resp(Http200, svgheaders, badge)

  get "/badges/repro/@pname/@arch/?@text?":
    ## Get reproducible status
    let pname = @"pname"
    let arch = @"arch"
    let rawstatus =
      retry:
        db.getvalue(sql"SELECT status FROM reproducible WHERE source=? AND architecture=?; ",
            pname, arch)
    let (status, color) =
      case rawstatus
      of "reproducible":
        ("yes", colGreen)
      of "depwait":
        ("depwait", colOrange)
      else:
        ("no", colRed)

    let text = @"text"
    let sbj = if text != "": text else: "reproducible"
    let badge = render_badge(sbj, status, color)
    resp(Http200, svgheaders, badge)


  get "/robots.txt":
    resp "User-agent: *\nDisallow: /badges/"


proc main() =
  log.info("Starting distrobadges")
  parse_defaults()
  let port = 7790.Port
  let settings = newSettings(port = port)
  var jester = initJester(myrouter, settings = settings)
  connect_to_db()
  jester.serve()

onSignal(SIGABRT):
  ## Handle SIGABRT from systemd
  log.debug("Received SIGABRT")
  quit(1)

when isMainModule:
  main()
