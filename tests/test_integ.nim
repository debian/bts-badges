# Debian BTS badges integration tests
# Copyright 2020 Federico Ceratto <federico@debian.org>
# Released under AGPLv3 License, see LICENSE file

import httpclient,
 strutils,
 unittest

const baseurl = "http://localhost:7790"

proc get(path: string): string =
  let url = baseurl & path
  echo "  fetching $#" % url
  return newHttpClient().getContent(url)


suite "basics":

  test "index":
    let page = get ""
    check page.contains "hi"

suite "wnpp":

  test "nis":
    let page = get "/badges/wnpp/nis/wnpp"
    check page.contains "orphaned"

  test "nim":
    let page = get "/badges/wnpp/nim/wnpp"
    check page.contains "done"

  test "bookie":
    let page = get "/badges/wnpp/bookie"
    check page.contains "RFP"

  test "mastodon":
    let page = get "/badges/wnpp/mastodon"
    check page.contains "ITP"

  test "nonexistent":
    let page = get "/badges/wnpp/nonexistent"
    check page.contains "none"

suite "repro":

  test "nis":
    let page = get "/badges/repro/nis/amd64"
    check page.contains "yes"

  test "nis":
    let page = get "/badges/repro/nim/amd64"
    check page.contains "yes"
