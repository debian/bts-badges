# Package

version       = "0.0.1"
author        = "Federico Ceratto"
description   = "Debian BTS badges"
license       = "AGPLv3"
bin           = @["btsbadges"]

# Dependencies

requires "nim >= 0.16.0", "morelogging", "jester"

task build_prod, "Build production release":
  exec "nim c -d:release -d:systemd btsbadges.nim"
